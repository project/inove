<?php
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>">
<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
</head>

<body>
<!-- wrap starts here -->
   <div id="wrap">
    <div id="container">	
      <!--header -->
      <div id="header">	
	<div id="headertext">
	  <div id="title">
	    <?php if ($site_name): ?> 	
	      <h1><a href="<?php print $base_path ?>" title="<?php print t('Home'); ?>"><?php print $site_name; ?></a></h1>		
	    <?php endif; ?>
	  </div>  <!-- end title -->
	  
	    <?php if ($site_slogan): ?>
	      <div id="tagline">
		<?php print $site_slogan ?>
	      </div>	
	    <?php endif; ?> 
	    
	    <?php print theme('links', $secondary_links, array('class' => 'secondary-links')) ?>
	</div> <!-- end headertext -->
	
      <!-- navigation -->
      <div id="navigation">
	<div  id="menus">
	    <?php print theme('links', $primary_links) ?>
	</div>	
      
	<div id="searchbox">
	    <?php print $search_box ?>
	</div>

	</div>						      
      </div>
		
		
      <!-- content starts here -->
      <div id="content">

	<div id="main">	 
	  
	    <?php if ($breadcrumb): ?>
	      <div id="postpath">
		<?php print $breadcrumb; ?>
		</div><!-- /postpath --> 
	      <?php endif ?>  
	    
	  
	  <?php print $tabs ?>
	  <?php if ($title) : ?>
	    <h1 class="title"><?php print $title ?></h1>
	  <?php endif ?>
	  <?php print $help ?>
	  <?php print $messages ?>
	  <?php print $content_top ?>
	  <?php print $content ?>
	  <?php print $content_bottom ?>
	  <?php print $feed_icons ?>
	</div>
	

	<div id="sidebar">
		<?php print $sidebar; ?>
		<?php print $left; ?>
	</div>
	
	<div class="clear">
	</div> <!-- end main -->
	
	
	
	<!--footer starts here-->
	<div id="footer">	
	  <a id="gotop" href="#">Top</a>
	
	    <?php print theme('links', $secondary_links, array('class' => 'secondary-links')) ?>
	    <?php if ($footer_message) : ?>
	      <?php print $footer_message ?>
	      <br />
	    <?php endif ?>
	      Design by: <a href="http://www.neoease.com/">mg12</a> | <a href="http://www.tauschen-ohne-geld.de/">Tauschb&ouml;rse</a>	
	  
	</div>

<!-- container ends here -->
</div>
<!-- wrap ends here -->
</div>
</div>
<?php print $closure ?>
</body>
</html>
