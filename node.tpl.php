<?php
?>
<div class="node<?php if ($sticky) { print " sticky"; } ?><?php if (!$status) { print " node-unpublished"; } ?>">

    <?php if ($page == 0) : ?>
      <h2 class="nodeTitle">
        <a href="<?php print $node_url; ?>">
          <?php print $title; ?>
        </a>
      </h2>
    <?php endif; ?>

    
    <div class="post">
    <?php if ($submitted): ?>
	<span class="date"><?php print $submitted ?></span>
    <?php endif; ?>
   


    <div class="content">
      <?php print $content; ?>
    </div>
    
 <?php print $links; ?>
    
    <?php if ($taxonomy) : ?>
      <div class="taxonomy">
        <?php print $terms; ?>
      </div>
    <?php endif; ?>
  </div>
</div>
    