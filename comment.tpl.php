<?php
?>
<div class="comment<?php print ' '. $status; ?>">
    <?php if ($new) : ?>
      <div class="comment-new">
	<?php print $new ?> 
      </div>
    <?php endif ?> 
    
 <div class="author">
  <div class="pic">  </div>
  <div class="name"> </div>
 </div>
 
 <div class="info">
  <div class="content"> 
    <h3 class="commentTitle"><?php print $title ?></h3>
    <?php if ($submitted) : ?>
      <div class="submitted">
	<?php print $submitted?>
      </div>
    <?php endif ?>
  
    <?php print $content; ?> 
    <?php if ($signature): ?>
      
      <div>—</div>
      <?php print $signature ?>
      </div>
    <?php endif ?>
    
    <?php if ($links) : ?>
      <div class="links">
	<?php print $links?>
      </div>
    <?php endif; ?>
  
  </div>
 </div>
 
 
</div>



